import axios, { AxiosInstance } from "axios";

import { AttachInitResponse, isAttachInitResponse } from "./types";

const BASE_URL =
  process.env.Q_ATTACH_API_BASE_URL || "https://www.qualified.io/api";

export class QualifiedAPI {
  private axios: AxiosInstance;
  private _token: string;

  constructor() {
    this.axios = axios.create({
      baseURL: BASE_URL,
      headers: {
        accept: "application/json",
        "content-type": "application/json",
      },
    });
    this._token = "";
  }

  set token(token: string) {
    this._token = token;
    this.axios.defaults.headers["Authorization"] = token;
  }

  get token() {
    return this._token;
  }

  // POST /attach to get challenge information to initialize
  async initAttach(): Promise<AttachInitResponse> {
    if (!this.token) throw new Error(`Missing token`);
    const { data } = await this.axios.post("/attach");
    if (!isAttachInitResponse(data)) {
      throw new TypeError(`Received unexpected response from /api/attach`);
    }
    return data;
  }

  // POST /attach/ably_token to refresh Ably token
  async refreshAblyToken(): Promise<string> {
    if (!this.token) throw new Error(`Missing token`);
    const { data } = await this.axios.post("/attach/ably_token");
    return data.token;
  }
}
