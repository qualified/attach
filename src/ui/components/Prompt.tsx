// Based on https://github.com/vadimdemedes/ink-text-input
import React, { useState } from "react";
import { Box, Color, Text, useInput } from "ink";
import chalk from "chalk";
import Status from "./Status";

const BACKSPACE = "\x08";
const DELETE = "\x7F";

export type PromptProps = {
  message: string;
  value?: string;
  placeholder?: string;
  defaultValue?: string;
  focus?: boolean;
  mask?: string;
  highlightPastedText?: boolean;
  showCursor?: boolean;
  onSubmit: (value: string) => void;
  validate?: (value: string) => true | string;
  transform?: (value: string) => string;
};

const Prompt = ({
  message,
  value: originalValue = "",
  placeholder = "",
  showCursor: originalShowCursor = true,
  focus = true,
  highlightPastedText = false,
  mask,
  onSubmit,
  validate = x => true,
  transform = x => x,
}: PromptProps) => {
  const [value, setValue] = useState(originalValue);
  const [showCursor, setShowCursor] = useState(originalShowCursor);
  const [cursorOffset, setCursorOffset] = useState(value.length);
  const [cursorWidth, setCursorWidth] = useState(0);
  const [error, setError] = useState("");

  useInput((input, key) => {
    if (!focus) return;
    if (key.upArrow || key.downArrow || (key.ctrl && input === "c")) return;
    if (key.return) {
      if (!value && placeholder) {
        setValue(placeholder);
        setShowCursor(false);
        onSubmit(transform(placeholder));
        return;
      }

      const valid = validate(value);
      if (valid === true) {
        setShowCursor(false);
        onSubmit(transform(value));
        return;
      }
      setError(valid);
      return;
    }

    let v = value;
    let offset = cursorOffset;
    let cWidth = 0;

    if (key.leftArrow) {
      if (showCursor && !mask) --offset;
    } else if (key.rightArrow) {
      if (showCursor && !mask) ++offset;
    } else if (input === BACKSPACE || input === DELETE) {
      v = v.slice(0, offset - 1) + v.slice(offset);
      --offset;
    } else {
      v = v.slice(0, offset) + input + v.slice(offset);
      offset += input.length;
      if (input.length > 1) cWidth = input.length;
    }

    offset = offset < 0 ? 0 : offset > v.length ? v.length : offset;
    if (offset !== cursorOffset) setCursorOffset(offset);
    if (cWidth !== cursorWidth) setCursorWidth(cWidth);
    if (v !== value) setValue(v);
    if (error) setError("");
  });

  const hasValue = value.length > 0;
  let renderedValue = value;
  if (mask) {
    renderedValue = mask.repeat(renderedValue.length);
  } else if (showCursor && focus) {
    // Fake mouse cursor, because it's too inconvenient to deal with actual cursor and ansi escapes
    if (!hasValue) {
      renderedValue = chalk.inverse(" ");
    } else {
      renderedValue = "";
      const cursorActualWidth = highlightPastedText ? cursorWidth : 0;
      let i = 0;
      // Use for...of to handle unicode properly
      for (const char of value) {
        if (i >= cursorOffset - cursorActualWidth && i <= cursorOffset) {
          renderedValue += chalk.inverse(char);
        } else {
          renderedValue += char;
        }
        ++i;
      }
      if (cursorOffset === i) renderedValue += chalk.inverse(" ");
    }
  }
  if (!hasValue && placeholder) renderedValue = placeholder;

  return (
    <Box flexDirection="column">
      {/* Break into multiple rows if the value is long to prevent overflowing to label */}
      <Box flexDirection={renderedValue.length > 80 ? "column" : "row"}>
        <Box paddingRight={1}>
          <Color green>Q</Color> <Text bold>{message}</Text>
        </Box>
        <Box textWrap="wrap">
          <Color dim={!hasValue && !!placeholder}>{renderedValue}</Color>
        </Box>
      </Box>
      {error ? <Status type="error" message={error} /> : null}
    </Box>
  );
};

export default Prompt;
