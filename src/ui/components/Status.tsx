import React from "react";
import { Box } from "ink";
import logSymbols from "log-symbols";

export type StatusProps = {
  type: keyof typeof logSymbols;
  message: string;
};

const Status = ({ type, message }: StatusProps) => {
  return (
    <Box>
      {logSymbols[type]} {message}
    </Box>
  );
};

export default Status;
