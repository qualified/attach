import React, { useState, useEffect } from "react";
import { Box, Text, useApp, useInput } from "ink";

import Intro from "./components/Intro";
import Spinner from "./components/Spinner";
import { Watcher } from "../watcher";
import { ChannelConnection } from "../connection";

export type AttachControllerProps = {
  watcher: Watcher;
  conn: ChannelConnection;
  resuming: boolean;
};

type ControllerState = "" | "testing" | "confirming" | "submitting";

// TODO Show warning and notification when the browser disconnects
const AttachController = ({
  watcher,
  conn,
  resuming,
}: AttachControllerProps) => {
  const { exit } = useApp();
  const [state, setState] = useState<ControllerState>("");

  useEffect(() => {
    // TODO Maybe show something on these events
    const added = watcher.added.subscribe(conn.fileAdded.bind(conn));
    const changed = watcher.changed.subscribe(conn.fileChanged.bind(conn));
    const removed = watcher.removed.subscribe(conn.fileRemoved.bind(conn));
    return () => {
      added.unsubscribe();
      changed.unsubscribe();
      removed.unsubscribe();
    };
  }, []);

  useInput((input, key) => {
    if (state === "submitting") return;
    if (state === "testing") return;

    if (state === "confirming") {
      if (input === "y" || input === "Y") {
        setState("submitting");
        conn.submit();
        setTimeout(() => {
          setState("");
          exit();
        }, 1500);
      } else {
        setState("");
      }
      return;
    }

    switch (input) {
      case "s":
        setState("confirming");
        break;

      case "t":
        setState("testing");
        conn.test();
        setTimeout(() => {
          setState("");
        }, 2000);
        break;

      case "q":
        exit();
        break;

      default:
        break;
    }
  });

  return (
    <Box flexDirection="column">
      <Intro workdirPath={watcher.cwd} resuming={resuming} />

      <Box marginTop={2} marginX={1} justifyContent="center">
        {getContentsForState(state)}
      </Box>
    </Box>
  );
};

function getContentsForState(state: ControllerState) {
  switch (state) {
    case "confirming":
      return (
        <Box>
          Press <Text bold>y</Text> to confirm submission. Anything else to
          cancel.
        </Box>
      );

    case "submitting":
      return <Spinner message="Submitting" />;

    case "testing":
      return <Spinner message="Triggering Tests" />;

    case "":
      return (
        <Box>
          {["test", "submit", "quit"].map(k => (
            <Box marginX={1} key={k}>
              <Text bold>{k[0]}</Text>:<Box marginLeft={1}>{k}</Box>
            </Box>
          ))}
        </Box>
      );
  }
}

export default AttachController;
