import React from "react";
import { render } from "ink";
import { eraseLines } from "ansi-escapes";

import Spinner from "./components/Spinner";
import Status, { StatusProps } from "./components/Status";

export function startSpinner(message: string) {
  const { rerender, unmount } = render(<Spinner message={message} />);

  const messageWithStatus = (type: StatusProps["type"]) => (
    message: string
  ) => {
    rerender(<Status type={type} message={message} />);
    unmount();
  };
  const stop = () => {
    unmount();
    // Erase the line with spinner. This assumes message is always one line.
    // Couldn't figure out how to do this better.
    // https://github.com/vadimdemedes/ink/issues/94
    process.stdout.write(eraseLines(2));
  };
  const update = (message: string) => {
    rerender(<Spinner message={message} />);
  };

  return {
    update,
    stop,
    succeed: messageWithStatus("success"),
    fail: messageWithStatus("error"),
    info: messageWithStatus("info"),
    warn: messageWithStatus("warning"),
  };
}

export async function withSpinner<T>(
  message: string,
  pending: Promise<T>
): Promise<T> {
  const { stop } = startSpinner(message);
  try {
    const result = await pending;
    stop();
    return result;
  } catch (e) {
    stop();
    throw e;
  }
}
