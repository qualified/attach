import { useEffect, useRef } from "react";

// https://overreacted.io/making-setinterval-declarative-with-react-hooks/
export type IntervalHandler = () => any;
function useInterval(callback: IntervalHandler, delay: number | null) {
  const ref = useRef<IntervalHandler>();

  // Remember the latest callback.
  useEffect(() => {
    ref.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    if (delay !== null) {
      const tick = () => {
        ref.current && ref.current();
      };
      const id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

export default useInterval;
